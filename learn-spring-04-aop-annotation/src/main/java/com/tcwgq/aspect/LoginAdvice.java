package com.tcwgq.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;

/**
 * @author tcwgq
 * @time 2017年8月20日上午9:31:39
 * @email tcwgq@outlook.com
 */
@Aspect
public class LoginAdvice {
    @Pointcut("execution(* com.tcwgq.service.LoginService.login*(..))")
    public void pointcut() {
    }

    @Before(value = "pointcut()")
    public void before() {
        System.out.println("login before...");
    }

    // 环绕通知
    @Around("pointcut()")
    public void around(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("login执行环绕通知开始...");
        // 调用目标方法
        joinPoint.proceed();
        System.out.println("login执行环绕通知结束...");
    }

    @AfterReturning(value = "pointcut()", returning = "result")
    public void afterReturning(int result) {
        System.out.println("login after-returning...");
        System.out.println("result = " + result);
    }

    @After("pointcut()")
    public void after() {
        System.out.println("login after...");
    }

}
