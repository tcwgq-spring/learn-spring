package com.tcwgq.model;

/**
 * @author tcwgq
 * @time 2017年8月20日上午9:30:29
 * @email tcwgq@outlook.com
 */
public class User {
    private String name;

    public User() {
    }

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "User [name=" + name + "]";
    }

}
