package com.tcwgq.aop;

import com.tcwgq.model.User;
import com.tcwgq.service.LoginService;
import com.tcwgq.service.UserService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by lenovo on 2017/8/20.
 */
public class AOPTest {
    @Test
    public void jdk() {
        // 只用注解配置aop和使用xml配置aop，增强方法的执行顺序存在问题
        ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        UserService userService = ctx.getBean(UserService.class);
        User user = new User("zhangSan");
        User add = userService.add(user);
        System.out.println("add user result:" + add);
    }

    @Test
    public void cglib() {
        // 只用注解配置aop和使用xml配置aop，增强方法的执行顺序存在问题
        ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        LoginService loginService = ctx.getBean(LoginService.class);
        User user = new User("zhangSan");
        User login = loginService.login(user);
        System.out.println("login user result:" + login);
    }
}
