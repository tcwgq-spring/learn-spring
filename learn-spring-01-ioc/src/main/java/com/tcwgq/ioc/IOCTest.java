package com.tcwgq.ioc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.tcwgq.model.User;

/**
 * @author tcwgq
 * @time 2017年8月13日下午10:27:42
 * @email tcwgq@outlook.com
 */
public class IOCTest {
	public static void main(String[] args) {
		ApplicationContext ctx = new ClassPathXmlApplicationContext(new String[] { "classpath:applicationContext.xml" });
		User user1 = (User) ctx.getBean("user");
		user1.add();
		System.out.println(user1);

		// User user2 = (User) ctx.getBean("user");
		// System.out.println(user2);
		// System.out.println(user1 == user2);
		// Person person = (Person) ctx.getBean("person");
		// System.out.println(person);
	}
}
