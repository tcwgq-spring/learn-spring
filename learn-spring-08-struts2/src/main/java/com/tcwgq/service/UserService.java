package com.tcwgq.service;

import com.tcwgq.dao.UserDao;

/**
 * @author tcwgq
 * @time 2017年8月20日上午11:14:44
 * @email tcwgq@outlook.com
 */
public class UserService {
	private UserDao userDao;

	public UserService() {

	}

	public UserService(UserDao userDao) {
		this.userDao = userDao;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public void add() {
		System.out.println("userService add...");
		userDao.add();
	}
}
