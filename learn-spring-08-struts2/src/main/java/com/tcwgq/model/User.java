package com.tcwgq.model;

/**
 * @author tcwgq
 * @time 2017年8月20日上午11:12:57
 * @email tcwgq@outlook.com
 */
public class User {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void add() {
		System.out.println("user add...");
	}

	@Override
	public String toString() {
		return "User [name=" + name + "]";
	}

}
