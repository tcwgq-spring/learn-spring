package com.tcwgq.model;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.*;

/**
 * @author tcwgq
 * @time 2017年8月13日下午10:25:22
 * @email tcwgq@outlook.com
 */
public class User implements BeanNameAware, BeanFactoryAware, InitializingBean, DisposableBean {
	private String username;

	public User() {
		super();
		System.out.println("user 无参构造方法...");
	}

	public User(String username) {
		super();
		this.username = username;
		System.out.println("user 有参构造方法...");
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		System.out.println("user setUsername...");
		this.username = username;
	}

	public void add() {
		System.out.println("user add...");
	}

	public void userinit() {
		System.out.println("user init...");
	}

	public void userdestroy() {
		System.out.println("user destroy...");
	}

	@Override
	public String toString() {
		return "User [username=" + username + "]";
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		System.out.println("InitializingBean afterPropertiesSet...");

	}

	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		System.out.println("BeanFactoryAware setBeanFactory...");
	}

	@Override
	public void setBeanName(String name) {
		System.out.println("BeanNameAware setBeanName...");
	}

	@Override
	public void destroy() throws Exception {
		System.out.println("DisposableBean destroy...");
	}

}
