package com.tcwgq.model;

import org.aspectj.lang.ProceedingJoinPoint;

/**
 * @author tcwgq
 * @time 2017年8月20日上午9:31:39
 * @email tcwgq@outlook.com
 */
public class UserAdvice {
	public void before() {
		System.out.println("add before...");
	}

	public void after() {
		System.out.println("add after...");
	}

	public void afterReturnning(int result) {
		System.out.println("add after-returnning...");
		System.out.println("result = " + result);
	}

	// 环绕通知
	public void around(ProceedingJoinPoint joinPoint) throws Throwable {
		System.out.println("执行环绕通知开始...");
		// 调用目标方法
		joinPoint.proceed();
		System.out.println("执行环绕通知结束...");
	}
}
