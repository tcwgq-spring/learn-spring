package com.tcwgq.model;

/**
 * @author tcwgq
 * @time 2017年8月20日上午9:30:29
 * @email tcwgq@outlook.com
 */
public class User {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int add() {
		System.out.println("user add...");
		return 100;
	}

	@Override
	public String toString() {
		return "User [name=" + name + "]";
	}

}
