package com.tcwgq.ioc;

import com.tcwgq.model.Person;
import com.tcwgq.model.User;
import com.tcwgq.service.BookService;
import com.tcwgq.service.UserService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author tcwgq
 * @time 2017年8月17日下午8:23:24
 * @email tcwgq@outlook.com
 */
public class IOCTest {
    /**
     * conponent controller service repository 四个注解功能相同，只是便于区分不同的类
     */
    @Test
    public void test() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        User user = (User) ctx.getBean("user");
        System.out.println(user);
        Person person = (Person) ctx.getBean("person");
        System.out.println(person);
        person = (Person) ctx.getBean("person");
        System.out.println(person);
    }

    @Test
    public void test1() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        UserService userService = (UserService) ctx.getBean("userService");
        userService.add();
    }

    @Test
    public void test2() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("beans.xml");
        BookService bookService = (BookService) ctx.getBean("bookService");
        bookService.add();
    }
}
