package com.tcwgq.service;

import javax.annotation.Resource;

import com.tcwgq.dao.BookDao;
import com.tcwgq.dao.OrderDao;

/**
 * @author tcwgq
 * @time 2017年8月18日上午8:10:39
 * @email tcwgq@outlook.com
 */
public class BookService {
	@Resource(name = "bookDao")
	private BookDao bookDao;

	@Resource(name = "orderDao")
	private OrderDao orderDao;

	public void add() {
		System.out.println("bookService add...");
		bookDao.add();
		orderDao.add();
	}
}
