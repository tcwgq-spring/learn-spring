package com.tcwgq.dao;

import org.springframework.stereotype.Repository;

/**
 * @author tcwgq
 * @time 2017年8月17日下午10:08:48
 * @email tcwgq@outlook.com
 */
@Repository("userDao123")
public class UserDao {
	public void add() {
		System.out.println("userDao add...");
	}
}
