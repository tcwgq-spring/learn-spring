package com.tcwgq.tx;

import java.beans.PropertyVetoException;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.tcwgq.service.UserService;

/**
 * 实际上还没有dbutils好用
 *
 * @author tcwgq
 * @time 2017年8月20日下午9:51:40
 * @email tcwgq@outlook.com
 */
public class TxTest {
	@Test
	public void testTranscation() throws PropertyVetoException {
		ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
		UserService userService = (UserService) ctx.getBean("userService");
		userService.transfer();
	}
}
