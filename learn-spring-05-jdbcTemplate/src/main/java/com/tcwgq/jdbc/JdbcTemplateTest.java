package com.tcwgq.jdbc;

import com.tcwgq.model.User;
import com.tcwgq.service.UserService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.beans.PropertyVetoException;
import java.sql.*;
import java.util.List;

/**
 * 实际上还没有dbutils好用
 *
 * @author tcwgq
 * @time 2017年8月20日下午9:51:40
 * @email tcwgq@outlook.com
 */
public class JdbcTemplateTest {

	@Test
	public void insert() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/learn-spring");
		dataSource.setUsername("root");
		dataSource.setPassword("112113");
		JdbcTemplate template = new JdbcTemplate(dataSource);
		String sql = "insert into t_user values(?, ?, ?)";
		for (int i = 1; i <= 10; i++) {
			int rows = template.update(sql, i, "zhangSan" + i, "112113");
			System.out.println(rows);
		}
	}

	@Test
	public void update() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/learn-spring");
		dataSource.setUsername("root");
		dataSource.setPassword("112113");
		JdbcTemplate template = new JdbcTemplate(dataSource);
		String sql = "update t_user set password = ? where id = ?";
		int rows = template.update(sql, "zhangSan", 1);
		System.out.println(rows);
	}

	@Test
	public void delete() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/learn-spring");
		dataSource.setUsername("root");
		dataSource.setPassword("112113");
		JdbcTemplate template = new JdbcTemplate(dataSource);
		String sql = "delete from t_user where id= ?";
		int rows = template.update(sql, 1);
		System.out.println(rows);
	}

	@Test
	public void selectCount() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/learn-spring");
		dataSource.setUsername("root");
		dataSource.setPassword("112113");
		JdbcTemplate template = new JdbcTemplate(dataSource);
		String sql = "select count(*) from t_user";
		Integer count = template.queryForObject(sql, Integer.class);
		System.out.println(count);
	}

	@Test
	public void testJdbc() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/learn-spring", "root", "112113");
		String sql = "select * from t_user where id = ?";
		PreparedStatement st = conn.prepareStatement(sql);
		st.setInt(1, 1);
		ResultSet rs = st.executeQuery();
		while (rs.next()) {
			System.out.println(rs.getString("username") + "--" + rs.getString("password"));
		}
	}

	@Test
	public void selectObject() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/learn-spring");
		dataSource.setUsername("root");
		dataSource.setPassword("112113");
		JdbcTemplate template = new JdbcTemplate(dataSource);
		String sql = "select * from t_user where id = ?";
		User user = template.queryForObject(sql, new RowMapper<User>() {

			@Override
			public User mapRow(ResultSet rs, int rowNum) throws SQLException {
				System.out.println(rowNum);
				return new User(rs.getInt(1), rs.getString(2), rs.getString(3));
			}

		}, 1);
		System.out.println(user);
	}

	@Test
	public void selectList() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/learn-spring");
		dataSource.setUsername("root");
		dataSource.setPassword("112113");
		JdbcTemplate template = new JdbcTemplate(dataSource);
		String sql = "select * from t_user where password = ?";
		List<User> users = template.query(sql, new RowMapper<User>() {

			@Override
			public User mapRow(ResultSet rs, int rowNum) throws SQLException {
				return new User(rs.getInt(1), rs.getString(2), rs.getString(3));
			}
		}, "112113");
		System.out.println(users);
	}

	@Test
	public void testC3p0() throws PropertyVetoException {
		// ComboPooledDataSource dataSource = new ComboPooledDataSource();
		// dataSource.setDriverClass("com.mysql.jdbc.Driver");
		// dataSource.setJdbcUrl("jdbc:mysql://localhost:3306/learn-spring");
		// dataSource.setUser("root");
		// dataSource.setPassword("112113");
		ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
		UserService userService = (UserService) ctx.getBean("userService");
		int rows = userService.add(100, "liSi", "1234");
		System.out.println(rows);
	}
}
